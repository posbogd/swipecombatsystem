﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatSystem  {
	public string outPutString;
	public enum SwipeType {ShortLeft,ShortRight,LongLeft,LongRight};
	public enum CombatAction {Attacking, Blocking};
	public void Attack(SwipeType st, CombatAction ca, float dmgValue)
	{
		
		switch (st) {
		case SwipeType.ShortLeft:

			if (ca == CombatAction.Attacking)
				ShortLeftAttack (dmgValue);
			else 
				ShortLeftBlock(dmgValue);

			break;
		
		case SwipeType.LongLeft:

			if (ca == CombatAction.Attacking)
				LongLeftAttack (dmgValue);
			else 
				LongLeftBlock(dmgValue);

			break;


		case SwipeType.ShortRight:
			
			if (ca == CombatAction.Attacking)
				ShortRightAttack (dmgValue);
			else 
				ShortRightBlock(dmgValue);
			break;

		case SwipeType.LongRight:

			if (ca == CombatAction.Attacking)
				LongRightAttack (dmgValue);
			else 
				LongRightBlock(dmgValue);

			break;
		}
	}

	//Attacks
	void LongLeftAttack(float dmgValue){
		outPutString=ConsoleOutput (dmgValue*2, CombatAction.Attacking,SwipeType.LongLeft);
	}
	void LongRightAttack(float dmgValue){
		outPutString=ConsoleOutput (dmgValue*2, CombatAction.Attacking,SwipeType.LongRight);
	}
	void ShortLeftAttack(float dmgValue){
		outPutString=ConsoleOutput (dmgValue, CombatAction.Attacking,SwipeType.ShortLeft);
	}
	void ShortRightAttack(float dmgValue){
		outPutString=ConsoleOutput (dmgValue, CombatAction.Attacking,SwipeType.ShortRight);
	}

	//Blocks 
	void LongLeftBlock(float dmgValue){
		outPutString=ConsoleOutput (dmgValue*2, CombatAction.Blocking, SwipeType.LongLeft);
	}
	void LongRightBlock(float dmgValue){
		outPutString=ConsoleOutput (dmgValue*2, CombatAction.Blocking,SwipeType.LongRight);
	}
	void ShortLeftBlock(float dmgValue){
		outPutString=ConsoleOutput (dmgValue, CombatAction.Blocking,SwipeType.ShortLeft);
	}
	void ShortRightBlock(float dmgValue){
		outPutString=ConsoleOutput (dmgValue, CombatAction.Blocking,SwipeType.ShortRight);
	}

	string ConsoleOutput(float dmgValue, CombatAction cm, SwipeType st){
		if (cm == CombatAction.Attacking)
			return dmgValue + " dmg dealt; direction - " + st + "; combo - ";
		else
			return dmgValue + " dmg blocked; direction - " + st + "; combo - ";
	}
}
