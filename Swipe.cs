﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Swipe : MonoBehaviour {
	public float shortSwipeLanght = 0.25f;
	public float longSwipeLanght = 0.5f;
	public Text consoleText;
	public GameObject go;
	List<string> console;


	bool isCombo;
	public float dmgValue=1;

	CombatSystem combatSystem;
	CombatSystem.CombatAction combatAction;
	CombatSystem.SwipeType swipeType;

	Combo combo;

	Vector2 touchStartPos;
	float centerOfScreenH;




	// Use this for initialization
	void Start () {
		combatSystem = new CombatSystem ();
		centerOfScreenH = (float) Screen.width / 2f;
		console = new List<string> ();
		combo = new Combo ();
		//comboT = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if (Input.GetMouseButton (0))
		{
					
			touchStartPos = Input.mousePosition;	
			consoleText.text = "" + Input.mousePosition;	;
			go.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0f));
			if (touchStartPos.x > centerOfScreenH)
				combatAction = CombatSystem.CombatAction.Blocking;
			else
				combatAction = CombatSystem.CombatAction.Attacking;
		}
		if (Input.GetMouseButton (1))
		{

			float horizontalDist = (new Vector2 (Input.mousePosition.x, 0) - new Vector2 (touchStartPos.x, 0)).magnitude;

			if (horizontalDist > shortSwipeL*Screen.width) {
				if (horizontalDist < longSwipeL*Screen.width) {
					if (Input.mousePosition.x - touchStartPos.x > 0)
						swipeType = CombatSystem.SwipeType.ShortLeft;
					else
						swipeType = CombatSystem.SwipeType.ShortRight;
				} else {
					if (Input.mousePosition.x - touchStartPos.x > 0)
						swipeType = CombatSystem.SwipeType.LongLeft;
					else
						swipeType = CombatSystem.SwipeType.LongRight;
				}
			}

			isCombo = CheckForCombo (combo, combatAction, Time.time);
			if (isCombo)
				combatSystem.Attack (swipeType, combatAction, dmgValue * 2);
			else
				combatSystem.Attack (swipeType, combatAction, dmgValue);

			UpdateConsoleText (combatSystem.outPutString);

			combo.SetCombo (swipeType,combatAction,Time.time);
			go.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,0f));

		}
		*/
		#if UNITY_ANDROID

		if(Input.touchCount>0)
		{
			
			if(IsLongSwipe(combo.swipeType))
			{
				if(Time.time - combo.lastActionTime > 2)
				{
					SwipeInputLogic();
				}
			} 
			else 
			SwipeInputLogic();

		}
		#endif
	}
	void SwipeInputLogic()
	{

		Touch touch = Input.touches [0];

		switch (touch.phase) {

		case TouchPhase.Began:

			touchStartPos = touch.position;


			if (touchStartPos.x > centerOfScreenH)
				combatAction = CombatSystem.CombatAction.Blocking;
			else
				combatAction = CombatSystem.CombatAction.Attacking;

			break;


		case TouchPhase.Ended:
			//float   verticalDist = (new Vector2 (0, touch.position.y) - new Vector2 (0, startPos.y)).magnitude;
			float horizontalDist = (new Vector2 (touch.position.x, 0) - new Vector2 (touchStartPos.x, 0)).magnitude;

			if (horizontalDist > shortSwipeLanght * Screen.width) {
				if (horizontalDist < longSwipeLanght * Screen.width) {
					if (touch.position.x - touchStartPos.x < 0)
						swipeType = CombatSystem.SwipeType.ShortLeft;
					else
						swipeType = CombatSystem.SwipeType.ShortRight;
				} else {
					if (touch.position.x - touchStartPos.x < 0)
						swipeType = CombatSystem.SwipeType.LongLeft;
					else
						swipeType = CombatSystem.SwipeType.LongRight;
				}

				isCombo = CheckForCombo (combo, combatAction, swipeType, Time.time);
				if (isCombo)
					combatSystem.Attack (swipeType, combatAction, dmgValue * 2);
				else
					combatSystem.Attack (swipeType, combatAction, dmgValue);

				UpdateConsoleText (combatSystem.outPutString);

				combo.SetCombo (swipeType, combatAction, Time.time);
				if (IsLongSwipe (swipeType))
					StartCoroutine (ActionsOnCd());
				;
			}


			break;
		}
	}
	IEnumerator ActionsOnCd()
	{
		go.SetActive (false);
		yield return new WaitForSeconds(2);
		go.SetActive (true);
	}
	bool CheckForCombo(Combo combo, CombatSystem.CombatAction thisAction,CombatSystem.SwipeType st,float time)
	{
		if (time - combo.lastActionTime < 1.5f) {
			if (combo.swipeType == CombatSystem.SwipeType.ShortLeft) {
				if (combo.combatAction != thisAction) {
					if(st==CombatSystem.SwipeType.LongRight)
					return true;
				}	
			}
		}
		return false;
	}
	void AddTextToConsole(string str)
	{	
		if (console.Count > 2)
			console.RemoveAt (0);
		console.Add (str+isCombo+"\n");
	}
	void UpdateConsoleText(string str)
	{
		string temp = null;
		AddTextToConsole (str);

		foreach(string line in console)
		{
			temp += line;
		}
		consoleText.text = temp;
	}
	bool IsLongSwipe(CombatSystem.SwipeType st)
	{
		if (st == CombatSystem.SwipeType.LongLeft || st == CombatSystem.SwipeType.LongRight) {
			return true;
		}
		return false;
	}

}

class Combo
{
	public CombatSystem.SwipeType swipeType;
	public CombatSystem.CombatAction combatAction;
	public float lastActionTime;

	public void SetCombo(CombatSystem.SwipeType st,CombatSystem.CombatAction ca,float time)
	{
		swipeType = st;
		combatAction = ca;
		lastActionTime = time;
	}
}
